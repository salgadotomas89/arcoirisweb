package com.ejemplo.ejemplo.repository;

import com.ejemplo.ejemplo.entity.Archivo;
import com.ejemplo.ejemplo.entity.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long> {
    @Query("select c from Curso c where c.id = :id")
    Curso getByID(@Param("id") Long id);
}
