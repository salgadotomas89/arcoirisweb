package com.ejemplo.ejemplo.repository;

import com.ejemplo.ejemplo.entity.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

}
