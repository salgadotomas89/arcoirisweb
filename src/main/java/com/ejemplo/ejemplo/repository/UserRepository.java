package com.ejemplo.ejemplo.repository;

import com.ejemplo.ejemplo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {
    @Query("select c from User c where c.username = ?1")
    User findByUsername( String username);

    @Query("select c from User c where c.id = ?1")
    User getById(Long id);

    @Modifying
    @Query("update User v set v.notificacion = ?2 where v.id = ?1 ")
    void updateNotification(Long id, int notificacion);
}
