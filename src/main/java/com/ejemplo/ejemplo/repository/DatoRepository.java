package com.ejemplo.ejemplo.repository;

import com.ejemplo.ejemplo.entity.FileArc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface DatoRepository extends JpaRepository<FileArc, Long> {

    @Modifying
    @Query("update FileArc u set u.contentType = ?2, u.datosDelArchivo = ?3, u.nombreArchivo = ?4 where u.id = ?1")
    void updateFileArc(Long idArchivoActualizar, String contentType, byte[] datosDelArchivo, String nombreArchivo);
}
