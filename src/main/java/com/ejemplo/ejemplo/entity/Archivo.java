package com.ejemplo.ejemplo.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.File;
import java.util.Date;

@Entity
public class Archivo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)//para el id automatico en la BD
    public Long id;

    public Long idUsuario;

    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    public FileArc filearc;

    public Boolean visible;

    public String nombreArchivo;

    public String des;

    public String estado;

    public Long nivel;

    public String tipo;

    public String comentario;

    public Date fecha;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public Long getNivel() {
        return nivel;
    }

    public void setNivel(Long nivel) {
        this.nivel = nivel;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public FileArc getFileArc() {
        return filearc;
    }

    public void setFileArc(FileArc fileArc) {
        this.filearc = fileArc;
    }


    public String getDescripcion() {
        return des;
    }

    public void setDescripcion(String descripcion) {
        this.des = descripcion;
    }
}
