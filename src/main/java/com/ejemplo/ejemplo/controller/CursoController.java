package com.ejemplo.ejemplo.controller;

import com.ejemplo.ejemplo.entity.Archivo;
import com.ejemplo.ejemplo.entity.Curso;
import com.ejemplo.ejemplo.entity.User;
import com.ejemplo.ejemplo.service.ArchivoService;
import com.ejemplo.ejemplo.service.CursoService;
import com.ejemplo.ejemplo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class CursoController {
    @Autowired
    UserService userService;
    @Autowired
    CursoService cursoService;
    @Autowired
    ArchivoService archivoService;

    Long idUsuario;


    @GetMapping("/cursos")
    public String cursos(Model model) {
        if(idUsuario == null){
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            UserDetails userDetails = null;
            if (principal instanceof UserDetails) {
                userDetails = (UserDetails) principal;
            }
            String userName = userDetails.getUsername();
            User user = userService.findByUsername(userName);
            idUsuario = user.getId();
        }
        model.addAttribute("notificacion", userService.get(idUsuario).getNotificacion());
        model.addAttribute("lista",cursoService.getAll());//lista con todos los cursos
        model.addAttribute("listaProfesores", userService.getAll());
        return "cursos";
    }

    @GetMapping("/savecurso/{id}")
    public String guardarCurso(@PathVariable("id") Long id, Model model) {
        if(id != null && id != 0){
            model.addAttribute("curso", cursoService.get(id));
        }else{
            model.addAttribute("curso", new Curso());
            model.addAttribute("listProfesores", userService.getAll());
        }
        return "add-curso";
    }

    @GetMapping("/deletecourse/{id}")
    public String borrarCurso(@PathVariable("id") Long id, Model model) {
        cursoService.delete(id);
        return "redirect:/cursos";
    }

    @PostMapping("/savecurso")
    public String saveCurso(Curso curso, Model model) {
        this.cursoService.save(curso);
        return "redirect:/cursos";
    }


}
