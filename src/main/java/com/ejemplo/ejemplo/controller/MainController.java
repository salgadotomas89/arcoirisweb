package com.ejemplo.ejemplo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MainController {

    @GetMapping("/login")
    public String login(){
        return "login";
    }

    @GetMapping("/intranet")
    public String intranet(HttpServletRequest request) {
        if (request.isUserInRole("ROLE_USER")) {
            return "redirect:/cursos";
        }else if(request.isUserInRole("ROLE_ADMIN")){
            return "redirect:/usuarios";
        }else {
            return "redirect:/login";
        }
    }

    @RequestMapping("/")
    public String index() {
        return "index";
    }


    @GetMapping("/recuperar")
    public String recuperarPass(Model model) {


        return "recuperar";
    }


    @GetMapping("/send/email")
    public void sendEmail(){

    }

}
