package com.ejemplo.ejemplo.controller;

import com.ejemplo.ejemplo.entity.Archivo;
import com.ejemplo.ejemplo.entity.Comentario;
import com.ejemplo.ejemplo.entity.FileArc;
import com.ejemplo.ejemplo.entity.User;
import com.ejemplo.ejemplo.service.ArchivoService;
import com.ejemplo.ejemplo.service.CursoService;
import com.ejemplo.ejemplo.service.DatoService;
import com.ejemplo.ejemplo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class ArchivoController {
    @Autowired
    ArchivoService archivoService;

    @Autowired
    DatoService datoService;

    @Autowired
    CursoService cursoService;

    @Autowired
    UserService userService;

    private Long idArchivoModificado;
    private Long idArchivoActualizar;
    private Long idUsuario = null;
    private Long idCursoElegido;
    private String tipoArchivo = "todo";

    @GetMapping("/comentario/{id}")
    public String updateComentario (@PathVariable Long id, Model model) {
        Comentario comentario = new Comentario();
        idArchivoModificado = id;
        comentario.setTexto(archivoService.get(id).getComentario());
        model.addAttribute("comen", comentario);
        return "add-comentario";
    }

    @GetMapping("/delete/file/{id}")
    public String deleteUser (@PathVariable Long id, Model model) {
        archivoService.delete(id);
        return "redirect:/board";
    }

    @GetMapping("/approvestate/file/{id}")
    public String apprveFile (@PathVariable Long id, Model model) {
        archivoService.updateEstado(id, "Aprobado");
        return "redirect:/board";
    }

    @GetMapping("/refusestate/file/{id}")
    public String refusefile (@PathVariable Long id, Model model) {
        archivoService.updateEstado(id, "Revisar!!");
        return "redirect:/board";
    }

    @GetMapping("/downloadfile/{id}")
    public ResponseEntity<Resource> downloadFile(@PathVariable Long id) throws IOException{
        /*
        Archivo dbFile = archivoService.get(id);
        InputStream targetStream = new FileInputStream(String.valueOf(dbFile.getArchivo()));
        InputStreamResource file = new InputStreamResource(targetStream);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getNombreArchivo() + "\"")
                .contentType(MediaType.parseMediaType(dbFile.getContentType()))
                .body(file);*/
        // Load file from database
        Archivo dbFile = archivoService.get(id);
        FileArc fileArc =  datoService.get(dbFile.getFileArc().getId());
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(fileArc.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getNombreArchivo() + "\"")
                .body(new ByteArrayResource(fileArc.getDatosDelArchivo()));
    }

    @GetMapping("/setnivel/{nivel}")
    public String cargarNivel(@PathVariable Long nivel){
        if(idUsuario == null){
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            UserDetails userDetails = null;
            if (principal instanceof UserDetails) {
                userDetails = (UserDetails) principal;
            }
            String userName = userDetails.getUsername();
            User user = userService.findByUsername(userName);
            idUsuario = user.getId();
        }
        idCursoElegido = nivel;
        return "redirect:/board";
    }

    @GetMapping("/setuser/{iduser}")
    public String cargarIdUsuario(@PathVariable Long iduser){
        idUsuario = iduser;
        return "redirect:/cursos";
    }

    @GetMapping("/settipo/{tipo}")
    public String cargarTipo(@PathVariable String tipo){
        tipoArchivo = tipo;
        return "redirect:/board";
    }

    @GetMapping("/notifications")
    public String notificaciones(Model model) {
        return "notificaciones";
    }



    @GetMapping("/board")
    public String tabla(Model model) {
        User user = userService.get(idUsuario);
        user.setNotificacion(0);
        userService.updateNotification(user);
        model.addAttribute("listaArchivos", archivoService.getAllByNivelId(idCursoElegido, idUsuario));
        model.addAttribute("curso", cursoService.getById(idCursoElegido));
        return "board";
    }

    @PostMapping("/uploadcomentario")
    public String setComentario(Comentario comentario, Model model){
        User usuario = userService.get(archivoService.get(idArchivoModificado).getIdUsuario());
        int notify = usuario.getNotificacion();
        notify ++;
        usuario.setNotificacion(notify);
        userService.updateNotification(usuario);
        archivoService.updateComentario(comentario, idArchivoModificado);
        return "redirect:/board";
    }

    @RequestMapping("/archivos")
    public String mostrarArchivos(Model model) {
        model.addAttribute("listaArchivos", archivoService.getAll());
        return "archivos";
    }

    @GetMapping("/addfile")
    public String guardarArchivo(Model model) {
        model.addAttribute("archivo", new Archivo());
        return "form-archivo";
    }

    @GetMapping("/up/{id}")
    public String uppp (@PathVariable Long id) {
        idArchivoActualizar = id;
        return "update-archivo";
    }

    @PostMapping("/update")
    public String updateFile (@RequestParam("file") MultipartFile file, Model model) {
        Archivo archivo = archivoService.get(idArchivoActualizar);
        FileArc fileArc =  datoService.get(archivo.getFileArc().getId());
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        fileArc.setContentType(file.getContentType());
        fileArc.setNombreArchivo(fileName);
        archivo.setNombreArchivo(fileName);
        try {
            fileArc.setDatosDelArchivo(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        archivoService.updateNombreArchivo(archivo);
        datoService.udpdatefileArc(idArchivoActualizar, fileArc);
        return "redirect:/board";
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam("des") String des, @RequestParam("tipo") String tipo, @RequestParam("file") MultipartFile file, RedirectAttributes attributes, Archivo archivo, Model model) {

        if (file.isEmpty()) {
            attributes.addFlashAttribute("message", "Elija un archivo válido.");
            return "redirect:/addfile";
        }

        /*
        if (!fileSizeIsHigher(file)) {
            attributes.addFlashAttribute("message", "Elija un archivo de tamaño menor a 40MB.");
            return "redirect:/addfile";
        }*/

        // normalize the file path
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        FileArc fileArc =  new FileArc();
        fileArc.setNombreArchivo(fileName);
        fileArc.setContentType(file.getContentType());
        archivo.setFecha(dameFecha());
        if(!des.isEmpty()) {
            archivo.setDescripcion(des);
        }
        archivo.setNivel(idCursoElegido);
        archivo.setIdUsuario(idUsuario);
        archivo.setNombreArchivo(fileName);
        archivo.setTipo(tipo);
        archivo.setFileArc(fileArc);
        try {
            fileArc.setDatosDelArchivo(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Path path = Paths.get(UPLOAD_DIR + fileName);
        //Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        datoService.save(fileArc);
        archivoService.save(archivo);

        attributes.addFlashAttribute("message", "You successfully uploaded " + fileName + '!');

        return "redirect:/board";
    }

    private Date dameFecha() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.format(date);
        return date;
    }







}
