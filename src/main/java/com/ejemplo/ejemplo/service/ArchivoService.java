package com.ejemplo.ejemplo.service;

import com.ejemplo.ejemplo.entity.Archivo;
import com.ejemplo.ejemplo.entity.Comentario;
import com.ejemplo.ejemplo.repository.ArchivoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ArchivoService {
    @Autowired
    public ArchivoRepository archivoRepository;

    public Object getAll() {
        return archivoRepository.findAll();
    }

    public Object getAllByNivel(Long nivel){
        return  archivoRepository.findAllByNivel(nivel);
    }

    public Object getAllByNivelAndTipo(Long nivel, String tipo){
        return archivoRepository.findAllByNivelAndTipo(nivel, tipo);
    }

    public Object getAllByNivelTipoId(Long nivel, String tipo, Long id){
        return archivoRepository.findAllByNivelTipoId(nivel, tipo, id);
    }

    public Archivo get(Long id) {
        return archivoRepository.findById(id).get();
    }

    public void save(Archivo archivo) {
        archivo.setEstado("Sin revisar");
        archivoRepository.save(archivo);
    }

    public void delete(Long id) {
        archivoRepository.deleteById(id);
    }

    public void updateComentario(Comentario comentario, Long id){
        Archivo archivo = archivoRepository.findById(id).get();
        archivo.setComentario(comentario.getTexto());
        archivoRepository.save(archivo);
    }


    public Object getAllByNivelId(Long nivelCurso, Long idUsuario) {
        return archivoRepository.findAllByNivelId(nivelCurso, idUsuario);
    }

    @Transactional
    public void updateEstado(Long id, String estado) {
        archivoRepository.updateEstado(id, estado);
    }

    @Transactional
    public void updateNombreArchivo(Archivo archivo) {
        archivoRepository.updateNombreArchivo(archivo.getId(), archivo.getNombreArchivo(), "Actualizado");
    }

    /*
    @Transactional
    public void updateArchivo(Long id, Archivo archivo) {
        archivoRepository.updateArchivoBytes(id, archivo.getArchivo(), archivo.getContentType(), archivo.getNombreArchivo());
    }*/


}
