package com.ejemplo.ejemplo.service;

import com.ejemplo.ejemplo.entity.Curso;
import com.ejemplo.ejemplo.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CursoService {
    @Autowired
    public CursoRepository cursoRepository;

    public Object getAll() {
        return cursoRepository.findAll();
    }

    public Object get(Long id) {
        return cursoRepository.findById(id);
    }

    public void save(Curso curso) {
        cursoRepository.save(curso);
    }

    public void delete(Long id) {
        cursoRepository.deleteById(id);
    }

    public Curso getById(Long id){
        return cursoRepository.getByID(id);
    }
}
