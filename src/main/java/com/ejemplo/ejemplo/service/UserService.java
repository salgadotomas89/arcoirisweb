package com.ejemplo.ejemplo.service;

import com.ejemplo.ejemplo.entity.Authority;
import com.ejemplo.ejemplo.entity.User;
import com.ejemplo.ejemplo.repository.AuthorityRepository;
import com.ejemplo.ejemplo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class UserService {

    @Autowired
    public UserRepository userRepository;
    @Autowired
    private AuthorityRepository authorityRepository;


    public Object getAll() {
        return userRepository.findAll();
    }

    public User get(Long id) {
        return userRepository.getById(id);
    }

    public void save(User user) {
        encriptarPassword(user);
        user.setNotificacion(0);
        user.setEnabled(true);
        user.setAuthority(new HashSet<>(authorityRepository.findAllById(Collections.singleton((long) 2))));
        userRepository.save(user );
    }

    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    private void encriptarPassword(User user){
        String pass = user.getPassword();
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(4);
        user.setPassword(bCryptPasswordEncoder.encode(pass));
    }

    @Transactional
    public void updateNotification(User usuario) {
        userRepository.updateNotification(usuario.getId(), usuario.getNotificacion());
    }
}
