package com.ejemplo.ejemplo.service;

import com.ejemplo.ejemplo.entity.Archivo;
import com.ejemplo.ejemplo.entity.FileArc;
import com.ejemplo.ejemplo.repository.DatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class DatoService {
    @Autowired
    DatoRepository datoRepository;

    public void save(FileArc fileArc){
        datoRepository.save(fileArc);
    }

    public FileArc get(Long id) {
        return datoRepository.findById(id).get();
    }

    @Transactional
    public void udpdatefileArc(Long idArchivoActualizar, FileArc fileArc) {
        datoRepository.updateFileArc(idArchivoActualizar, fileArc.getContentType(), fileArc.getDatosDelArchivo(), fileArc.getNombreArchivo());
    }

    public void delete(Long id) {
        datoRepository.deleteById(id);
    }
}
